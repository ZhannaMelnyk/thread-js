import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Label, Icon } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const Comment = ({
  comment,
  currentUserId,
  toggleEditedComment,
  likeComment,
  dislikeComment,
  postId,
  toggleDeletedComment }) => {
  const {
    id,
    body,
    user,
    createdAt,
    commentReactions,
    likeCount,
    dislikeCount
  } = comment;

  const initialLikeCount = commentReactions?.filter(cr => cr.isLike).length || 0;
  const initialDislikeCount = commentReactions?.filter(cr => !cr.isLike).length || 0;

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
      </CommentUI.Content>
      <CommentUI.Content>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likeComment(id, postId)}>
          <Icon name="thumbs up" />
          {likeCount || initialLikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikeComment(id, postId)}>
          <Icon name="thumbs down" />
          {dislikeCount || initialDislikeCount}
        </Label>
        {
          user.id === currentUserId
            ? (
              <>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleEditedComment(id)}>
                  <Icon name="edit" />
                </Label>
                <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleDeletedComment(id)}>
                  <Icon name="trash alternate" />
                </Label>
              </>
            )
            : null
        }
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  currentUserId: PropTypes.string.isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  postId: PropTypes.string.isRequired,
  toggleDeletedComment: PropTypes.func.isRequired
};

export default Comment;
