import React from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Form, Button } from 'semantic-ui-react';
import moment from 'moment';
import { getUserImgLink } from 'src/helpers/imageHelper';

import styles from './styles.module.scss';

const DeleteCommentForm = ({
  comment,
  toggleDeletedComment: toggle,
  deleteComment
}) => {
  const {
    id,
    body,
    user,
    createdAt
  } = comment;
  const handleDeleteComment = async () => {
    await deleteComment(id);
  };

  return (
    <>
      <CommentUI className={styles.comment}>
        <CommentUI.Avatar src={getUserImgLink(user.image)} className={styles.avatar} />
        <CommentUI>
          <CommentUI.Content>
            <CommentUI.Author as="a">
              {user.username}
            </CommentUI.Author>
            <CommentUI.Metadata as="span" className={styles.metadata}>
              {moment(createdAt).fromNow()}
            </CommentUI.Metadata>
            <CommentUI.Text>
              {body}
            </CommentUI.Text>
          </CommentUI.Content>
        </CommentUI>
      </CommentUI>
      <Form reply onSubmit={() => { handleDeleteComment(); toggle(); }}>
        <Button type="submit" content="Yes" labelPosition="left" icon="trash alternate" primary />
        <Button content="No" labelPosition="left" icon="delete" secondary onClick={() => toggle()} />
      </Form>
    </>
  );
};

DeleteCommentForm.propTypes = {
  deleteComment: PropTypes.func.isRequired,
  toggleDeletedComment: PropTypes.func.isRequired,
  comment: PropTypes.objectOf(PropTypes.any).isRequired
};

export default DeleteCommentForm;
