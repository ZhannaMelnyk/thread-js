import React from 'react';
import PropTypes from 'prop-types';
import { Card, Image, Form, Button } from 'semantic-ui-react';
import moment from 'moment';

const DeletePostForm = ({
  post,
  deletePost,
  toggleDeletedPost: toggle
}) => {
  const {
    image,
    body,
    user,
    createdAt
  } = post;
  const date = moment(createdAt).fromNow();
  const handleDeletePost = async () => {
    await deletePost(post.id);
  };

  return (
    <Form reply onSubmit={() => { handleDeletePost(); toggle(); }}>
      <Card style={{ width: '100%' }}>
        {image && <Image src={image.link} wrapped ui={false} />}
        <Card.Content>
          <Card.Meta>
            <span className="date">
              posted by
              {' '}
              {user?.username}
              {' - '}
              {date}
            </span>
          </Card.Meta>
          <Card.Description>
            {body}
          </Card.Description>
        </Card.Content>
      </Card>
      <Button type="submit" content="Yes" labelPosition="left" icon="trash alternate" primary />
      <Button content="No" labelPosition="left" icon="delete" secondary onClick={() => toggle()} />
    </Form>
  );
};

DeletePostForm.propTypes = {
  deletePost: PropTypes.func.isRequired,
  toggleDeletedPost: PropTypes.func.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired
};

export default DeletePostForm;
