import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const EditCommentForm = ({
  comment,
  updateEditedComment,
  toggleEditedComment: toggle
}) => {
  const [body, setBody] = useState(comment.body);

  const handleEditComment = async () => {
    if (!body) {
      return;
    }
    await updateEditedComment(comment.id, body);
  };

  return (
    <Form reply onSubmit={() => { handleEditComment(); toggle(); }}>
      <Form.TextArea
        value={body}
        placeholder="Edit a post..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Edit comment" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

EditCommentForm.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  updateEditedComment: PropTypes.func.isRequired,
  toggleEditedComment: PropTypes.func.isRequired
};

export default EditCommentForm;
