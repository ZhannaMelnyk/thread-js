import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

const EditPostForm = ({
  post,
  toggleEditedPost: toggle,
  updateEditedPost
}) => {
  const [body, setBody] = useState(post.body);

  const handleEditPost = async () => {
    if (!body) {
      return;
    }
    await updateEditedPost(post.id, body);
  };

  return (
    <Form reply onSubmit={() => { handleEditPost(); toggle(); }}>
      <Form.TextArea
        value={body}
        placeholder="Edit a post..."
        onChange={ev => setBody(ev.target.value)}
      />
      <Button type="submit" content="Edit post" labelPosition="left" icon="edit" primary />
    </Form>
  );
};

EditPostForm.propTypes = {
  updateEditedPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  post: PropTypes.objectOf(PropTypes.any).isRequired
};

export default EditPostForm;
