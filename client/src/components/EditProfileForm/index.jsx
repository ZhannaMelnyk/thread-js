import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button, Input, Image, Icon } from 'semantic-ui-react';
import { getUserImgLink } from 'src/helpers/imageHelper';
import * as imageService from 'src/services/imageService';

const EditProfileForm = ({
  user,
  alreadyExistUserName,
  toggleEditedUser: toggle,
  editUser: edit,
  toggleExistUsernameToUndefined: toggleExistUsername
}) => {
  const [userName, setUserName] = useState(user.username);
  const [isUploading, setIsUploading] = useState(false);
  const [avatar, setAvatar] = useState(undefined);

  const handleEditProfile = async () => {
    if (!userName) {
      return;
    }
    const isChanged = await edit(user.id, user.username, userName, avatar);
    if (isChanged) {
      toggle();
    }
  };

  const uploadImage = file => imageService.uploadImage(file);

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);
    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setAvatar({ imageId, imageLink });
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Form reply onSubmit={() => handleEditProfile(userName)}>
      <Image src={avatar?.imageLink || getUserImgLink(user.image)} size="medium" circular />
      <br />
      <Button icon color="teal" labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
        <Icon name="image" />
        Attach image
        <input name="image" type="file" onChange={handleUploadFile} hidden />
      </Button>
      <br />
      <br />
      <Input
        icon="user"
        iconPosition="left"
        placeholder="Username"
        type="text"
        value={userName}
        onChange={ev => { setUserName(ev.target.value); toggleExistUsername(); }}
      />
      <br />
      {alreadyExistUserName
        ? (
          <>
            <span>this username already exists</span>
            <br />
            <br />
            <Button type="submit" content="Unable to save" labelPosition="left" icon="edit" disabled primary />
          </>
        )
        : (
          <>
            <br />
            <Button type="submit" content="Save changes" labelPosition="left" icon="edit" primary />
          </>
        )}
    </Form>
  );
};

EditProfileForm.propTypes = {
  toggleEditedUser: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  editUser: PropTypes.func.isRequired,
  alreadyExistUserName: PropTypes.string,
  toggleExistUsernameToUndefined: PropTypes.func.isRequired
};

EditProfileForm.defaultProps = {
  alreadyExistUserName: undefined
};

export default EditProfileForm;
