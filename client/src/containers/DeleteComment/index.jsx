import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Header } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';
import DeleteCommentForm from 'src/components/DeleteCommentForm';

const DeleteComment = ({
  comment,
  toggleDeletedComment: toggle,
  deleteComment: commentDelete
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {comment
      ? (
        <Modal.Content>
          <Header as="h3" dividing>
            Are you sure you want to delete this comment?
          </Header>
          <DeleteCommentForm comment={comment} deleteComment={commentDelete} toggleDeletedComment={toggle} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

DeleteComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleDeletedComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

export default DeleteComment;
