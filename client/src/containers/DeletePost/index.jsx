import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Header } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';
import DeletePostForm from 'src/components/DeletePostForm';

const DeletePost = ({
  post,
  toggleDeletedPost: toggle,
  deletePost: postDelete
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Header as="h3" dividing>
            Are you sure you want to delete this post?
          </Header>
          <DeletePostForm post={post} deletePost={postDelete} toggleDeletedPost={toggle} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

DeletePost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleDeletedPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired
};

export default DeletePost;
