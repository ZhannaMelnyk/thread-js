import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Header } from 'semantic-ui-react';
import EditCommentForm from 'src/components/EditCommentForm';
import Spinner from 'src/components/Spinner';

const EditComment = ({
  comment,
  toggleEditedComment: toggle,
  updateEditedComment: update
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {comment
      ? (
        <Modal.Content>
          <Header as="h3" dividing>
            Edit Comment
          </Header>
          <EditCommentForm comment={comment} updateEditedComment={update} toggleEditedComment={toggle} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EditComment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  updateEditedComment: PropTypes.func.isRequired
};

export default EditComment;
