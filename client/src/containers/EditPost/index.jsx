import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Header } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';
import EditPostForm from 'src/components/EditPostForm';

const EditPost = ({
  post,
  toggleEditedPost: toggle,
  updateEditedPost: update
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Header as="h3" dividing>
            Edit post
          </Header>
          <EditPostForm post={post} updateEditedPost={update} toggleEditedPost={toggle} />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EditPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  updateEditedPost: PropTypes.func.isRequired
};

export default EditPost;
