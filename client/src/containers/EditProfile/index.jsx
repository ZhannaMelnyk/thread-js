import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Header } from 'semantic-ui-react';
import Spinner from 'src/components/Spinner';
import EditPostForm from 'src/components/EditProfileForm';

const EditProfile = ({
  user,
  alreadyExistUserName,
  toggleEditedUser: toggle,
  editUser: edit,
  toggleExistUsernameToUndefined: toggleExistUsername
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {user
      ? (
        <Modal.Content>
          <Header as="h3" dividing>
            Edit profile
          </Header>
          <EditPostForm
            user={user}
            toggleEditedUser={toggle}
            editUser={edit}
            alreadyExistUserName={alreadyExistUserName}
            toggleExistUsernameToUndefined={toggleExistUsername}
          />
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

EditProfile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleEditedUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired,
  alreadyExistUserName: PropTypes.string,
  toggleExistUsernameToUndefined: PropTypes.func.isRequired
};

EditProfile.defaultProps = {
  alreadyExistUserName: undefined
};

export default EditProfile;
