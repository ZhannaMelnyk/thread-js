import React from 'react';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Modal, Comment as CommentUI, Header } from 'semantic-ui-react';
import moment from 'moment';
import {
  likePost,
  dislikePost,
  toggleExpandedPost,
  addComment,
  toggleEditedPost,
  toggleEditedComment
} from 'src/containers/Thread/actions';
import Post from 'src/components/Post';
import Comment from 'src/components/Comment';
import AddComment from 'src/components/AddComment';
import Spinner from 'src/components/Spinner';

const ExpandedPost = ({
  post,
  sharePost,
  userId,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggle,
  addComment: add,
  toggleEditedPost: toggleEdited,
  toggleEditedComment: toggleUpdatedComment,
  toggleDeletedPost: toggleDeleted,
  toggleDeletedComment: toggleCommentDeleted,
  likeComment: commentLike,
  dislikeComment: commentDislike
}) => (
  <Modal dimmer="blurring" centered={false} open onClose={() => toggle()}>
    {post
      ? (
        <Modal.Content>
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            togglePost={toggle}
            sharePost={sharePost}
            toggleEditedPost={toggleEdited}
            currentUserId={userId}
            toggleDeletedPost={toggleDeleted}
          />
          <CommentUI.Group style={{ maxWidth: '100%' }}>
            <Header as="h3" dividing>
              Comments
            </Header>
            {post.comments && post.comments
              .sort((c1, c2) => moment(c1.createdAt).diff(c2.createdAt))
              .map(comment => (
                <Comment
                  postId={post.id}
                  key={comment.id}
                  comment={comment}
                  currentUserId={userId}
                  toggleEditedComment={toggleUpdatedComment}
                  likeComment={commentLike}
                  dislikeComment={commentDislike}
                  toggleDeletedComment={toggleCommentDeleted}
                  toggleExpandedPost={toggle}
                />
              ))}
            <AddComment postId={post.id} addComment={add} />
          </CommentUI.Group>
        </Modal.Content>
      )
      : <Spinner />}
  </Modal>
);

ExpandedPost.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  toggleDeletedPost: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  addComment: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  userId: PropTypes.string.isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  toggleDeletedComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired
};

const mapStateToProps = rootState => ({
  post: rootState.posts.expandedPost
});

const actions = { likePost, dislikePost, toggleExpandedPost, addComment, toggleEditedPost, toggleEditedComment };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ExpandedPost);
