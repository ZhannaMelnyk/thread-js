import * as authService from 'src/services/authService';
import { SET_USER, SET_EDITED_USER, SET_ALREADY_EXIST_USER } from './actionTypes';

const setToken = token => localStorage.setItem('token', token);

const setUser = user => async dispatch => dispatch({
  type: SET_USER,
  user
});

const setEditedUser = user => async dispatch => dispatch({
  type: SET_EDITED_USER,
  user
});

const setAlreadyExistUser = userName => async dispatch => dispatch({
  type: SET_ALREADY_EXIST_USER,
  userName
});

const setAuthData = (user = null, token = '') => (dispatch, getRootState) => {
  setToken(token); // token should be set first before user
  setUser(user)(dispatch, getRootState);
};

const handleAuthResponse = authResponsePromise => async (dispatch, getRootState) => {
  const { user, token } = await authResponsePromise;
  setAuthData(user, token)(dispatch, getRootState);
};

export const login = request => handleAuthResponse(authService.login(request));

export const register = request => handleAuthResponse(authService.registration(request));

export const logout = () => setAuthData();

export const loadCurrentUser = () => async (dispatch, getRootState) => {
  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
};

export const toggleEditedUser = () => async (dispatch, getRootState) => {
  const { profile: { editedUser } } = getRootState();
  const user = editedUser ? undefined : await authService.getCurrentUser();
  setEditedUser(user)(dispatch, getRootState);
};

export const editUser = (userId, oldUserName, newUserName, avatar = '') => async (dispatch, getRootState) => {
  const { id } = await authService.checkUserName(newUserName);
  let isChanged;

  if (id && oldUserName !== newUserName) {
    setAlreadyExistUser(newUserName)(dispatch, getRootState);
    isChanged = false;
  } else {
    await authService.editUser(userId, newUserName, avatar);
    isChanged = true;
  }

  const user = await authService.getCurrentUser();
  setUser(user)(dispatch, getRootState);
  return isChanged;
};

export const toggleExistUsernameToUndefined = () => (dispatch, getRootState) => {
  setAlreadyExistUser(undefined)(dispatch, getRootState);
};
