import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import {
  Grid,
  Image,
  Input,
  Form,
  Button
} from 'semantic-ui-react';
import EditProfile from 'src/containers/EditProfile';
import { toggleEditedUser, editUser, toggleExistUsernameToUndefined } from './actions';

const Profile = ({
  user,
  editedUser,
  alreadyExistUserName,
  toggleEditedUser: toggle,
  editUser: edit,
  toggleExistUsernameToUndefined: toggleExistUsername
}) => (
  <div>
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <Image centered src={getUserImgLink(user.image)} size="medium" circular />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          disabled
          value={user.username}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <Form reply style={{ paddingTop: 20 }} onSubmit={() => toggle()}>
          <Button type="submit" content="Edit profile" labelPosition="left" icon="edit" primary />
        </Form>
      </Grid.Column>
    </Grid>
    {editedUser
      && (
        <EditProfile
          user={editedUser}
          toggleEditedUser={toggle}
          editUser={edit}
          alreadyExistUserName={alreadyExistUserName}
          toggleExistUsernameToUndefined={toggleExistUsername}
        />
      )}
  </div>
);

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  editedUser: PropTypes.objectOf(PropTypes.any),
  alreadyExistUserName: PropTypes.string,
  toggleEditedUser: PropTypes.func.isRequired,
  editUser: PropTypes.func.isRequired,
  toggleExistUsernameToUndefined: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {},
  editedUser: undefined,
  alreadyExistUserName: undefined
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user,
  editedUser: rootState.profile.editedUser,
  alreadyExistUserName: rootState.profile.alreadyExistUserName
});

const actions = { toggleEditedUser, editUser, toggleExistUsernameToUndefined };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
