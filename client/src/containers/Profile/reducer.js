import { SET_USER, SET_EDITED_USER, SET_ALREADY_EXIST_USER } from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_USER:
      return {
        ...state,
        user: action.user,
        isAuthorized: Boolean(action.user?.id),
        isLoading: false
      };
    case SET_EDITED_USER:
      return {
        ...state,
        editedUser: action.user
      };
    case SET_ALREADY_EXIST_USER:
      return {
        ...state,
        alreadyExistUserName: action.userName
      };
    default:
      return state;
  }
};
