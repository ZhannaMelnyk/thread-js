import * as postService from 'src/services/postService';
import * as commentService from 'src/services/commentService';
import {
  ADD_POST,
  LOAD_MORE_POSTS,
  SET_ALL_POSTS,
  SET_EXPANDED_POST,
  SET_EDITED_POST,
  SET_EDITED_COMMENT,
  SET_DELETED_POST,
  SET_DELETED_COMMENT
} from './actionTypes';

const setPostsAction = posts => ({
  type: SET_ALL_POSTS,
  posts
});

const addMorePostsAction = posts => ({
  type: LOAD_MORE_POSTS,
  posts
});

const addPostAction = post => ({
  type: ADD_POST,
  post
});

const setExpandedPostAction = post => ({
  type: SET_EXPANDED_POST,
  post
});

const setEditedPostAction = post => ({
  type: SET_EDITED_POST,
  post
});

const setEditedCommentAction = comment => ({
  type: SET_EDITED_COMMENT,
  comment
});

const setDeletedPostAction = post => ({
  type: SET_DELETED_POST,
  post
});

const setDeletedCommentAction = comment => ({
  type: SET_DELETED_COMMENT,
  comment
});

export const loadPosts = filter => async dispatch => {
  const posts = await postService.getAllPosts(filter);
  dispatch(setPostsAction(posts));
};

export const loadMorePosts = filter => async (dispatch, getRootState) => {
  const { posts: { posts } } = getRootState();
  const loadedPosts = await postService.getAllPosts(filter);
  const filteredPosts = loadedPosts
    .filter(post => !(posts && posts.some(loadedPost => post.id === loadedPost.id)));
  dispatch(addMorePostsAction(filteredPosts));
};

export const applyPost = postId => async dispatch => {
  const post = await postService.getPost(postId);
  dispatch(addPostAction(post));
};

export const addPost = post => async dispatch => {
  const { id } = await postService.addPost(post);
  const newPost = await postService.getPost(id);
  dispatch(addPostAction(newPost));
};

export const toggleExpandedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setExpandedPostAction(post));
};

export const likePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.likePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was liked, otherwise - like was removed

  const { dislikeCount } = await postService.getPost(postId);

  const mapLikes = post => ({
    ...post,
    likeCount: Number(post.likeCount) + diff, // diff is taken from the current closure
    dislikeCount
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapLikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapLikes(expandedPost)));
  }
};

export const dislikePost = postId => async (dispatch, getRootState) => {
  const { id } = await postService.dislikePost(postId);
  const diff = id ? 1 : -1; // if ID exists then the post was disliked, otherwise - dislike was removed

  const { likeCount } = await postService.getPost(postId);

  const mapDislikes = post => ({
    ...post,
    dislikeCount: Number(post.dislikeCount) + diff, // diff is taken from the current closure
    likeCount
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== postId ? post : mapDislikes(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === postId) {
    dispatch(setExpandedPostAction(mapDislikes(expandedPost)));
  }
};

export const addComment = request => async (dispatch, getRootState) => {
  const { id } = await commentService.addComment(request);
  const comment = await commentService.getComment(id);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) + 1,
    comments: [...(post.comments || []), comment] // comment is taken from the current closure
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== comment.postId
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};

export const toggleEditedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setEditedPostAction(post));
};

export const updateEditedPost = (postId, newBody) => async dispatch => {
  await postService.updatePost(postId, newBody);

  const posts = await postService.getAllPosts();
  dispatch(setPostsAction(posts));
};

export const toggleEditedComment = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setEditedCommentAction(comment));
};

export const updateEditedComment = (commentId, newBody) => async (dispatch, getRootState) => {
  const { id } = await commentService.updateComment(commentId, newBody);
  const comment = await commentService.getComment(id);

  const mapEditedComment = post => ({
    ...post,
    comments: [...post.comments.filter(currentComment => currentComment.id !== id), comment]
  });

  const { posts: { expandedPost } } = getRootState();

  if (expandedPost && expandedPost.id === comment.postId) {
    dispatch(setExpandedPostAction(mapEditedComment(expandedPost)));
  }
};

export const likeComment = (commentId, postId) => async (dispatch, getRootState) => {
  await commentService.likeComment(commentId);

  const { comments } = await postService.getPost(postId);
  const currentComment = comments.find(c => c.id === commentId);
  const likeCount = currentComment.commentReactions.filter(cr => cr.isLike).length;
  const dislikeCount = currentComment.commentReactions.filter(cr => !cr.isLike).length;

  const mapLikes = comment => ({
    ...comment,
    likeCount,
    dislikeCount
  });

  const { posts: { expandedPost } } = getRootState();

  const updateExpandedPost = post => ({
    ...post,
    comments: comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)))
  });

  dispatch(setExpandedPostAction(updateExpandedPost(expandedPost)));
};

export const dislikeComment = (commentId, postId) => async (dispatch, getRootState) => {
  await commentService.dislikeComment(commentId);

  const { comments } = await postService.getPost(postId);
  const currentComment = comments.find(c => c.id === commentId);
  const likeCount = currentComment.commentReactions.filter(cr => cr.isLike === true).length;
  const dislikeCount = currentComment.commentReactions.filter(cr => cr.isLike === false).length;

  const mapLikes = comment => ({
    ...comment,
    likeCount,
    dislikeCount
  });

  const { posts: { expandedPost } } = getRootState();

  const updateExpandedPost = post => ({
    ...post,
    comments: comments.map(comment => (comment.id !== commentId ? comment : mapLikes(comment)))
  });

  dispatch(setExpandedPostAction(updateExpandedPost(expandedPost)));
};

export const toggleDeletedPost = postId => async dispatch => {
  const post = postId ? await postService.getPost(postId) : undefined;
  dispatch(setDeletedPostAction(post));
};

export const deletePost = postId => async dispatch => {
  await postService.deletePost(postId);

  const posts = await postService.getAllPosts();
  dispatch(setPostsAction(posts));
};

export const toggleDeletedComment = commentId => async dispatch => {
  const comment = commentId ? await commentService.getComment(commentId) : undefined;
  dispatch(setDeletedCommentAction(comment));
};

export const deleteComment = commentId => async (dispatch, getRootState) => {
  const { id } = await commentService.deleteComment(commentId);

  const mapComments = post => ({
    ...post,
    commentCount: Number(post.commentCount) - 1,
    comments: [...post.comments.filter(currentComment => currentComment.id !== id)]
  });

  const { posts: { posts, expandedPost } } = getRootState();
  const updated = posts.map(post => (post.id !== expandedPost.id
    ? post
    : mapComments(post)));

  dispatch(setPostsAction(updated));

  if (expandedPost) {
    dispatch(setExpandedPostAction(mapComments(expandedPost)));
  }
};
