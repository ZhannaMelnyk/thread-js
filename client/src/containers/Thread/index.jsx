/* eslint-disable no-unused-vars */
import React, { useState, useEffect } from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import * as imageService from 'src/services/imageService';
import ExpandedPost from 'src/containers/ExpandedPost';
import EditPost from 'src/containers/EditPost';
import EditComment from 'src/containers/EditComment';
import Post from 'src/components/Post';
import AddPost from 'src/components/AddPost';
import DeletePost from 'src/containers/DeletePost';
import DeleteComment from 'src/containers/DeleteComment';
import SharedPostLink from 'src/components/SharedPostLink';
import { Radio, Loader } from 'semantic-ui-react';
import InfiniteScroll from 'react-infinite-scroller';
import {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  toggleEditedPost,
  updateEditedPost,
  toggleEditedComment,
  updateEditedComment,
  likeComment,
  dislikeComment,
  toggleDeletedPost,
  deletePost,
  toggleDeletedComment,
  deleteComment
} from './actions';

import styles from './styles.module.scss';

const postsFilter = {
  userId: undefined,
  exceptUserId: undefined,
  likedBy: undefined,
  from: 0,
  count: 10
};

const Thread = ({
  userId,
  loadPosts: load,
  loadMorePosts: loadMore,
  posts = [],
  expandedPost,
  hasMorePosts,
  editedPost,
  deletedPost,
  editedComment,
  deletedComment,
  addPost: createPost,
  likePost: like,
  dislikePost: dislike,
  toggleExpandedPost: toggleExpanded,
  toggleEditedPost: toggleEdited,
  updateEditedPost: updatePost,
  toggleEditedComment: toggleUpdatedComment,
  updateEditedComment: updateComment,
  likeComment: commentLike,
  dislikeComment: commentDislike,
  toggleDeletedPost: toggleDeleted,
  deletePost: postDelete,
  toggleDeletedComment: toggleCommentDeleted,
  deleteComment: commentDelete
}) => {
  const [sharedPostId, setSharedPostId] = useState(undefined);
  const [showAllPosts, setShowAllPosts] = useState(true);
  const [showOwnPosts, setShowOwnPosts] = useState(false);
  const [hideOwnPosts, setHideOwnPosts] = useState(false);
  const [showLikedByMe, setShowLikedByMe] = useState(false);

  const toggleShowAllPosts = () => {
    setShowAllPosts(true);
    setShowOwnPosts(false);
    setHideOwnPosts(false);
    setShowLikedByMe(false);
    postsFilter.userId = undefined;
    postsFilter.exceptUserId = undefined;
    postsFilter.likedBy = undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowOwnPosts = () => {
    setShowOwnPosts(true);
    setShowAllPosts(false);
    setHideOwnPosts(false);
    setShowLikedByMe(false);
    postsFilter.userId = showOwnPosts ? undefined : userId;
    postsFilter.exceptUserId = undefined;
    postsFilter.likedBy = undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleHideOwnPosts = () => {
    setHideOwnPosts(true);
    setShowAllPosts(false);
    setShowOwnPosts(false);
    setShowLikedByMe(false);
    postsFilter.userId = undefined;
    postsFilter.exceptUserId = hideOwnPosts ? undefined : userId;
    postsFilter.likedBy = undefined;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const toggleShowLikedByMe = () => {
    setShowLikedByMe(true);
    setHideOwnPosts(false);
    setShowAllPosts(false);
    setShowOwnPosts(false);
    postsFilter.userId = undefined;
    postsFilter.exceptUserId = undefined;
    postsFilter.likedBy = showLikedByMe ? undefined : userId;
    postsFilter.from = 0;
    load(postsFilter);
    postsFilter.from = postsFilter.count; // for the next scroll
  };

  const getMorePosts = () => {
    loadMore(postsFilter);
    const { from, count } = postsFilter;
    postsFilter.from = from + count;
  };

  const sharePost = id => {
    setSharedPostId(id);
  };

  const uploadImage = file => imageService.uploadImage(file);

  return (
    <div className={styles.threadContent}>
      <div className={styles.addPostForm}>
        <AddPost addPost={createPost} uploadImage={uploadImage} />
      </div>
      <div className={styles.toolbar}>
        <Radio
          label="Show all posts"
          checked={showAllPosts}
          onChange={toggleShowAllPosts}
        />
        <Radio
          label="Show only my posts"
          checked={showOwnPosts}
          onChange={toggleShowOwnPosts}
        />
        <Radio
          label="Hide my posts"
          checked={hideOwnPosts}
          onChange={toggleHideOwnPosts}
        />
        <Radio
          label="Show liked by me"
          checked={showLikedByMe}
          onChange={toggleShowLikedByMe}
        />
      </div>
      <InfiniteScroll
        pageStart={0}
        loadMore={getMorePosts}
        hasMore={hasMorePosts}
        loader={<Loader active inline="centered" key={0} />}
      >
        {posts.map(post => (
          <Post
            post={post}
            likePost={like}
            dislikePost={dislike}
            toggleExpandedPost={toggleExpanded}
            sharePost={sharePost}
            key={post.id}
            currentUserId={userId}
            toggleEditedPost={toggleEdited}
            toggleDeletedPost={toggleDeleted}
          />
        ))}
      </InfiniteScroll>
      {expandedPost
        && (
          <ExpandedPost
            sharePost={sharePost}
            userId={userId}
            toggleEditedComment={toggleUpdatedComment}
            likeComment={commentLike}
            dislikeComment={commentDislike}
            toggleDeletedPost={toggleDeleted}
            toggleDeletedComment={toggleCommentDeleted}
          />
        )}
      {sharedPostId && <SharedPostLink postId={sharedPostId} close={() => setSharedPostId(undefined)} />}
      {editedPost
        && (
          <EditPost
            post={editedPost}
            toggleEditedPost={toggleEdited}
            updateEditedPost={updatePost}
          />
        )}
      {editedComment
        && (
          <EditComment
            comment={editedComment}
            toggleEditedComment={toggleUpdatedComment}
            updateEditedComment={updateComment}
          />
        )}
      {deletedPost
        && (
          <DeletePost
            post={deletedPost}
            toggleDeletedPost={toggleDeleted}
            deletePost={postDelete}
          />
        )}
      {deletedComment
        && (
          <DeleteComment
            comment={deletedComment}
            toggleDeletedComment={toggleCommentDeleted}
            deleteComment={commentDelete}
          />
        )}
    </div>
  );
};

Thread.propTypes = {
  posts: PropTypes.arrayOf(PropTypes.object),
  hasMorePosts: PropTypes.bool,
  expandedPost: PropTypes.objectOf(PropTypes.any),
  editedPost: PropTypes.objectOf(PropTypes.any),
  editedComment: PropTypes.objectOf(PropTypes.any),
  deletedPost: PropTypes.objectOf(PropTypes.any),
  deletedComment: PropTypes.objectOf(PropTypes.any),
  userId: PropTypes.string,
  loadPosts: PropTypes.func.isRequired,
  loadMorePosts: PropTypes.func.isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  addPost: PropTypes.func.isRequired,
  toggleEditedPost: PropTypes.func.isRequired,
  updateEditedPost: PropTypes.func.isRequired,
  toggleEditedComment: PropTypes.func.isRequired,
  updateEditedComment: PropTypes.func.isRequired,
  likeComment: PropTypes.func.isRequired,
  dislikeComment: PropTypes.func.isRequired,
  toggleDeletedPost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  toggleDeletedComment: PropTypes.func.isRequired,
  deleteComment: PropTypes.func.isRequired
};

Thread.defaultProps = {
  posts: [],
  hasMorePosts: true,
  expandedPost: undefined,
  userId: undefined,
  editedPost: undefined,
  editedComment: undefined,
  deletedPost: undefined,
  deletedComment: undefined
};

const mapStateToProps = rootState => ({
  posts: rootState.posts.posts,
  hasMorePosts: rootState.posts.hasMorePosts,
  expandedPost: rootState.posts.expandedPost,
  userId: rootState.profile.user.id,
  editedPost: rootState.posts.editedPost,
  editedComment: rootState.posts.editedComment,
  deletedPost: rootState.posts.deletedPost,
  deletedComment: rootState.posts.deletedComment
});

const actions = {
  loadPosts,
  loadMorePosts,
  likePost,
  dislikePost,
  toggleExpandedPost,
  addPost,
  toggleEditedPost,
  updateEditedPost,
  toggleEditedComment,
  updateEditedComment,
  likeComment,
  dislikeComment,
  toggleDeletedPost,
  deletePost,
  toggleDeletedComment,
  deleteComment
};

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Thread);
