import { Router } from 'express';
import * as commentService from '../services/commentService';

const router = Router();

router
  .get('/:id', (req, res, next) => commentService.getCommentById(req.params.id)
    .then(comment => res.send(comment))
    .catch(next))
  .post('/', (req, res, next) => commentService.create(req.user.id, req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .put('/react', (req, res, next) => commentService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.comment && (reaction.comment.userId !== req.user.id)) {
        if (req.body.isLike) {
          // notify a user if someone (not himself) liked his comment
          req.io.to(reaction.comment.userId).emit('likeComment', 'Your comment was liked!');
        } else {
          // notify a user if someone (not himself) disliked his comment
          req.io.to(reaction.comment.userId).emit('dislikeComment', 'Your post was disliked!');
        }
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => commentService.updateComment(req.body)
    .then(comment => res.send(comment))
    .catch(next))
  .delete('/:id', (req, res, next) => commentService.deleteComment(req.params.id)
    .then(post => res.send(post))
    .catch(next));

export default router;
