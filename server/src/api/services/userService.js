import userRepository from '../../data/repositories/userRepository';

export const getUserById = async userId => {
  const { id, username, email, imageId, image } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image };
};

export const checkUserName = async userName => userRepository.getByUsername(userName);

export const updateUser = ({ userId, username, imageId }) => {
  const updatedUser = userRepository.updateById(userId, { username, imageId });
  return updatedUser;
};
