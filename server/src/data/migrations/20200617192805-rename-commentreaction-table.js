export default {
  up: queryInterface => queryInterface.renameTable('commentReaction', 'commentReactions'),

  down: queryInterface => queryInterface.renameTable('commentReactions', 'commentReaction')
};
