/* eslint-disable no-console */
import commentReactionSeed from '../seed-data/commentReactionsSeed';

const randomIndex = length => Math.floor(Math.random() * length);

export default {
  up: async (queryInterface, Sequelize) => {
    try {
      const options = {
        type: Sequelize.QueryTypes.SELECT
      };

      const comments = await queryInterface.sequelize.query('SELECT id FROM "comments";', options);
      const users = await queryInterface.sequelize.query('SELECT id FROM "users";', options);

      const commentReactionMappedSeed = commentReactionSeed.map(reaction => ({
        ...reaction,
        userId: users[randomIndex(users.length)].id,
        commentId: comments[randomIndex(comments.length)].id
      }));
      await queryInterface.bulkInsert('commentReaction', commentReactionMappedSeed, {});
    } catch (err) {
      console.log(`Seeding error: ${err}`);
    }
  },

  down: async queryInterface => {
    try {
      await queryInterface.bulkDelete('commentReaction', null, {});
    } catch (err) {
      console.log(`Seeding error: ${err}`);
    }
  }
};
